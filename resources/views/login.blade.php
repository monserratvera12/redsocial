<!doctype html>
<html lang="es">
<!--
Ejemplo de formulario de login con Bootstrap
Basado en:
Plantilla inicial de Bootstrap 4
@author parzibyte
Visita: parzibyte.me/blog
-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,
            shrink-to-fit=no">
    <meta name="description" content="Formulario de login con Bootstrap">
    <meta name="author" content="Parzibyte">
    <title>INICIO DE SESION</title>
    <!-- Cargar el CSS de Boostrap-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
<main role="main" class="container my-auto">
    <div class="row">
        <div id="login" class="col-lg-4 offset-lg-4 col-md-6 offset-md-3
                    col-12">
            <h2 class="text-center">Bienvenido de nuevo</h2>
            <img class="img-fluid mx-auto d-block rounded"
                 src="https://assets.entrepreneur.com/content/3x2/2000/social_network.jpg" />

            <form class="user" action="{{route('login.form')}}" method="post">
                @if(isset($estatus))
                    @if($estatus == "success")
                        <label class="text-primary">{{$mensaje}}</label>
                    @elseif($estatus == "error")
                        <label class="text-danger">{{$mensaje}}</label>
                    @endif
                @endif
                {{csrf_field()}}
                <div class="form-group">
                    <label for="correo">Correo</label>
                    <input id="correo" name="correo"
                           class="form-control" type="email"
                           placeholder="Correo electrónico">
                </div>
                <div class="form-group">
                    <label for="palabraSecreta">Contraseña</label>
                    <input id="palabraSecreta" name="password"
                           class="form-control" type="password"
                           placeholder="Contraseña">
                </div>
                <br>
                <button type="submit" class="form-control btn btn-primary mb-2">
                    Entrar
                </button>
                <br>
                    @if(isset($_GET["r"]))
                        <input type="hidden" name="url" value="{{$_GET["r"]}}">
                    @endif
                <a href="{{route('registro')}}">Crear Cuenta</a>
            </form>
        </div>
    </div>
</main>
</body>

</html>
